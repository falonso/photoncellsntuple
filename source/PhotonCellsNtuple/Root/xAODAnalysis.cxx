#include <AsgMessaging/MessageCheck.h>
#include <PhotonCellsNtuple/xAODAnalysis.h>

#include "CaloUtils/CaloLayerCalculator.h"

#include <fstream>

xAODAnalysis :: xAODAnalysis (const std::string& name,
                              ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
      m_calo_tool("CaloFillRectangularCluster/CaloFillRectangularCluster_7x11", this)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.

    declareProperty("EventsFile", m_events_file);
    declareProperty("IsMC", m_isMC);

    declareProperty("calo_tool", m_calo_tool, "The xxx");
}

StatusCode xAODAnalysis :: initialize ()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    m_previous_run_number = 0;

    // output tree
    ANA_CHECK(setup_tree());

    // counts histogram
    ANA_CHECK (book (TH1F ("h_counts", "h_counts", 7, 0, 7)));
    hist("h_counts")->GetXaxis()->SetBinLabel(1, "# all events");
    hist("h_counts")->GetXaxis()->SetBinLabel(2, "# events matched");
    hist("h_counts")->GetXaxis()->SetBinLabel(3, "# events with cell information");
    hist("h_counts")->GetXaxis()->SetBinLabel(4, "# L1 ok");
    hist("h_counts")->GetXaxis()->SetBinLabel(5, "# L2 ok");
    hist("h_counts")->GetXaxis()->SetBinLabel(6, "# L3 ok");
    hist("h_counts")->GetXaxis()->SetBinLabel(7, "# All layers ok");

    // Calo tool
    ATH_CHECK(m_SGKey_caloCells.initialize());
    ATH_CHECK(m_calo_tool.retrieve());

    // ATH_CHECK(m_inputClusterCollection.initialize());
    // ATH_CHECK(m_caloDetDescrMgrKey.initialize());
    // ATH_CHECK(m_cellsKey.initialize());

    return StatusCode::SUCCESS;
}

StatusCode xAODAnalysis :: execute ()
{
    ANA_CHECK(clear_variables());

    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

    // Event number selection
    run_number = eventInfo->runNumber();
    event_number = eventInfo->eventNumber();

    if (run_number != m_previous_run_number) {
        ANA_CHECK(read_events_file(run_number));
        m_previous_run_number = run_number;
    }

    if ( !std::count(m_selected_events.begin(), m_selected_events.end(), event_number) )
        return StatusCode::SUCCESS;


    ANA_MSG_INFO("Processing event " << event_number);

    hist("h_counts")->Fill(0.5);

    // Selected photon to match
    MatchPhoton sel_photon = m_selected_photons[event_number];

    // Photon selection
    const xAOD::PhotonContainer *photons(0);
    ANA_CHECK(evtStore()->retrieve(photons, "Photons"));

    m_photons.clear();

    double eta, phi, dR2, matched_dR2 = 999;
    unsigned int matched_idx = -1;
    for (const auto& ph : *photons) {

        eta = ( ph->caloCluster() ) ? ph->caloCluster()->etaBE(2) : -999.0;
        phi = ph->phi();

        if (ph->pt() < 5000.)
            continue;
        if (fabs(eta) > 2.4)
            continue;

        //int ph_origin = ph->auxdata<int>("truthOrigin");
        //int ph_type = ph->auxdata<int>("truthType");

        // Try to match photon with the selected one in the official framework using dR
        dR2 = (eta - sel_photon.eta) * (eta - sel_photon.eta) + (phi - sel_photon.phi) * (phi - sel_photon.phi);

        if (dR2  > 0.1)
            continue;

        m_photons.push_back(ph);

        if (dR2 < matched_dR2) {
            matched_dR2 = dR2;
            matched_idx = m_photons.size() - 1;
        }

    }

    if (m_photons.size() == 0) {
        ANA_MSG_WARNING("Match failed! No photon matched with (eta, phi) = (" << sel_photon.eta << "," << sel_photon.phi << "). Skipping event ... ");
        return StatusCode::SUCCESS;
    }

    if (m_photons.size() > 1) {
        ANA_MSG_INFO("Match not unique. " << m_photons.size() << " photons matched.");
        ANA_MSG_INFO("Selected photon (eta, phi) = (" << sel_photon.eta << "," << sel_photon.phi << ")");
        ANA_MSG_INFO("Matched photons (pt, eta, phi): (" << m_photons[0]->pt()/1000. << "," << m_photons[0]->caloCluster()->etaBE(2) << "," << m_photons[0]->phi() << "), (" <<
                     m_photons[1]->pt()/1000. << "," << m_photons[1]->caloCluster()->etaBE(2) << "," << m_photons[1]->phi() << ")");
        ANA_MSG_INFO("Selecting closest photon with dR = " << TMath::Sqrt(matched_dR2) << "... ");
    }

    matched_idx = 0;
    const xAOD::Photon *photon = m_photons[matched_idx];

    ANA_MSG_DEBUG("Matched photon: (pt, eta, phi) = " << Form("(%.2f, %.2f, %.2f)", ph_pt, ph_eta, ph_phi) << " - Original photon: (pt, eta, phi) = " << Form("(%.2f, %.2f)", sel_photon.eta, sel_photon.phi));

    ph_pt  = photon->pt() * 0.001;
    ph_eta = ( photon->caloCluster() ) ? photon->caloCluster()->etaBE(2) : -999.0;
    ph_phi = photon->phi();

    hist("h_counts")->Fill(1.5);

    // Fill cells info
    if (fill_cluster_info(photon).isFailure()) {
        ATH_MSG_WARNING("Failed filling cell info, skip event");
        return StatusCode::SUCCESS;
    }

    hist("h_counts")->Fill(2.5);

    // Fill eta/phi/E for each layer
    ph_L1_eta = photon->caloCluster()->etaBE(1);
    ph_L2_eta = photon->caloCluster()->etaBE(2);
    ph_L3_eta = photon->caloCluster()->etaBE(3);
    ph_L1_phi = photon->caloCluster()->phiBE(1);
    ph_L2_phi = photon->caloCluster()->phiBE(2);
    ph_L3_phi = photon->caloCluster()->phiBE(3);
    ph_L1_E = photon->caloCluster()->energyBE(1);
    ph_L2_E = photon->caloCluster()->energyBE(2);
    ph_L3_E = photon->caloCluster()->energyBE(3);

    // Fill shower shape details
    ph_ss_e132 = photon->showerShapeValue(xAOD::EgammaParameters::e132);
    ph_ss_e1152 = photon->showerShapeValue(xAOD::EgammaParameters::e1152);
    ph_ss_ethad1 = photon->showerShapeValue(xAOD::EgammaParameters::ethad1);
    ph_ss_ethad = photon->showerShapeValue(xAOD::EgammaParameters::ethad);
    ph_ss_ehad1 = photon->showerShapeValue(xAOD::EgammaParameters::ehad1);
    ph_ss_f1 = photon->showerShapeValue(xAOD::EgammaParameters::f1);
    ph_ss_f3 = photon->showerShapeValue(xAOD::EgammaParameters::f3);
    ph_ss_f1core = photon->showerShapeValue(xAOD::EgammaParameters::f1core);
    ph_ss_f3core = photon->showerShapeValue(xAOD::EgammaParameters::f3core);
    ph_ss_e233 = photon->showerShapeValue(xAOD::EgammaParameters::e233);
    ph_ss_e235 = photon->showerShapeValue(xAOD::EgammaParameters::e235);
    ph_ss_e255 = photon->showerShapeValue(xAOD::EgammaParameters::e255);
    ph_ss_e237 = photon->showerShapeValue(xAOD::EgammaParameters::e237);
    ph_ss_e277 = photon->showerShapeValue(xAOD::EgammaParameters::e277);
    ph_ss_weta1 = photon->showerShapeValue(xAOD::EgammaParameters::weta1);
    ph_ss_weta2 = photon->showerShapeValue(xAOD::EgammaParameters::weta2);
    ph_ss_e2ts1 = photon->showerShapeValue(xAOD::EgammaParameters::e2ts1);
    ph_ss_e2tsts1 = photon->showerShapeValue(xAOD::EgammaParameters::e2tsts1);
    ph_ss_fracs1 = photon->showerShapeValue(xAOD::EgammaParameters::fracs1);
    ph_ss_widths1 = photon->showerShapeValue(xAOD::EgammaParameters::widths1);
    ph_ss_widths2 = photon->showerShapeValue(xAOD::EgammaParameters::widths2);
    ph_ss_poscs1 = photon->showerShapeValue(xAOD::EgammaParameters::poscs1);
    ph_ss_poscs2 = photon->showerShapeValue(xAOD::EgammaParameters::poscs2);
    ph_ss_asy1 = photon->showerShapeValue(xAOD::EgammaParameters::asy1);
    ph_ss_pos = photon->showerShapeValue(xAOD::EgammaParameters::pos);
    ph_ss_pos7 = photon->showerShapeValue(xAOD::EgammaParameters::pos7);
    ph_ss_barys1 = photon->showerShapeValue(xAOD::EgammaParameters::barys1);
    ph_ss_wtots1 = photon->showerShapeValue(xAOD::EgammaParameters::wtots1);
    ph_ss_emins1 = photon->showerShapeValue(xAOD::EgammaParameters::emins1);
    ph_ss_emaxs1 = photon->showerShapeValue(xAOD::EgammaParameters::emaxs1);
    ph_ss_r33over37allcalo = photon->showerShapeValue(xAOD::EgammaParameters::r33over37allcalo);
    ph_ss_ecore = photon->showerShapeValue(xAOD::EgammaParameters::ecore);
    ph_ss_Reta = photon->showerShapeValue(xAOD::EgammaParameters::Reta);
    ph_ss_Rphi = photon->showerShapeValue(xAOD::EgammaParameters::Rphi);
    ph_ss_Eratio = photon->showerShapeValue(xAOD::EgammaParameters::Eratio);
    ph_ss_Rhad = photon->showerShapeValue(xAOD::EgammaParameters::Rhad);
    ph_ss_Rhad1 = photon->showerShapeValue(xAOD::EgammaParameters::Rhad1);
    ph_ss_DeltaE = photon->showerShapeValue(xAOD::EgammaParameters::DeltaE);

    ph_author = photon->author();
    ph_ambiguity_type = static_cast<int>(photon->auxdata<uint8_t>("ambiguityType"));

    tree("cells")->Fill();

  return StatusCode::SUCCESS;
}


StatusCode xAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

StatusCode xAODAnalysis :: clear_variables()
{
    run_number = 0;
    event_number = 0;

    ph_pt  = 0.;
    ph_eta = 0.;
    ph_phi = 0.;

    ph_cl_eta0 = 0.;
    ph_cl_phi0 = 0.;
    ph_cl_eta  = 0.;
    ph_cl_phi  = 0.;
    ph_calo_eta  = 0.;
    ph_calo_phi  = 0.;
    ph_delta_eta = 0.;
    ph_delta_phi = 0.;

    ph_cells_L0_size = 0;
    ph_cells_L1_size = 0;
    ph_cells_L2_size = 0;
    ph_cells_L3_size = 0;

    ph_cells_L0_E.clear();
    ph_cells_L0_eta.clear();
    ph_cells_L0_phi.clear();

    ph_cells_L1_E.clear();
    ph_cells_L1_eta.clear();
    ph_cells_L1_phi.clear();
    ph_cells_L1_etar.clear();
    ph_cells_L1_phir.clear();
    ph_cells_L1_incl.clear();

    ph_cells_L2_E.clear();
    ph_cells_L2_eta.clear();
    ph_cells_L2_phi.clear();
    ph_cells_L2_etar.clear();
    ph_cells_L2_phir.clear();
    ph_cells_L2_incl.clear();

    ph_cells_L3_E.clear();
    ph_cells_L3_eta.clear();
    ph_cells_L3_phi.clear();
    ph_cells_L3_etar.clear();
    ph_cells_L3_phir.clear();
    ph_cells_L3_incl.clear();

    // some extra variables no included in main ntuples
    ph_L1_E = 0.;
    ph_L2_E = 0.;
    ph_L3_E = 0.;
    ph_L1_eta = 0.;
    ph_L2_eta = 0.;
    ph_L3_eta = 0.;
    ph_L1_phi = 0.;
    ph_L2_phi = 0.;
    ph_L3_phi = 0.;

    ph_ss_e132 = 0.;
    ph_ss_e1152 = 0.;
    ph_ss_ethad1 = 0.;
    ph_ss_ethad = 0.;
    ph_ss_ehad1 = 0.;
    ph_ss_f1 = 0.;
    ph_ss_f3 = 0.;
    ph_ss_f1core = 0.;
    ph_ss_f3core = 0.;
    ph_ss_e233 = 0.;
    ph_ss_e235 = 0.;
    ph_ss_e255 = 0.;
    ph_ss_e237 = 0.;
    ph_ss_e277 = 0.;
    ph_ss_weta1 = 0.;
    ph_ss_weta2 = 0.;
    ph_ss_e2ts1 = 0.;
    ph_ss_e2tsts1 = 0.;
    ph_ss_fracs1 = 0.;
    ph_ss_widths1 = 0.;
    ph_ss_widths2 = 0.;
    ph_ss_poscs1 = 0.;
    ph_ss_poscs2 = 0.;
    ph_ss_asy1 = 0.;
    ph_ss_pos = 0.;
    ph_ss_pos7 = 0.;
    ph_ss_barys1 = 0.;
    ph_ss_wtots1 = 0.;
    ph_ss_emins1 = 0.;
    ph_ss_emaxs1 = 0.;
    ph_ss_r33over37allcalo = 0.;
    ph_ss_ecore = 0.;
    ph_ss_Reta = 0.;
    ph_ss_Rphi = 0.;
    ph_ss_Eratio = 0.;
    ph_ss_Rhad = 0.;
    ph_ss_Rhad1 = 0.;
    ph_ss_DeltaE = 0.;

    return StatusCode::SUCCESS;
}


StatusCode xAODAnalysis :: setup_tree()
{
    ANA_CHECK(book(TTree("cells", "cells")));

    TTree *tc = tree("cells");

    tc->Branch("RunNumber", &run_number);
    tc->Branch("EventNumber", &event_number);

    tc->Branch("ph_pt", &ph_pt);
    tc->Branch("ph_eta", &ph_eta);
    tc->Branch("ph_phi", &ph_phi);

    // cluster eta/phi
    tc->Branch("ph_cl_eta0", &ph_cl_eta0);
    tc->Branch("ph_cl_phi0", &ph_cl_phi0);
    tc->Branch("ph_cl_eta", &ph_cl_eta);
    tc->Branch("ph_cl_phi", &ph_cl_phi);
    tc->Branch("ph_calo_eta", &ph_calo_eta);
    tc->Branch("ph_calo_phi", &ph_calo_phi);
    tc->Branch("ph_delta_eta", &ph_delta_eta);
    tc->Branch("ph_delta_phi", &ph_delta_phi);

    tc->Branch("ph_cells_7x11_L0_size",  &ph_cells_L0_size);
    tc->Branch("ph_cells_7x11_L1_size",  &ph_cells_L1_size);
    tc->Branch("ph_cells_7x11_L2_size",  &ph_cells_L2_size);
    tc->Branch("ph_cells_7x11_L3_size",  &ph_cells_L3_size);

    tc->Branch("ph_cells_7x11_L0_E"  ,   &ph_cells_L0_E);
    tc->Branch("ph_cells_7x11_L0_eta",   &ph_cells_L0_eta);
    tc->Branch("ph_cells_7x11_L0_phi",   &ph_cells_L0_phi);

    tc->Branch("ph_cells_7x11_L1_E"  ,   &ph_cells_L1_E);
    tc->Branch("ph_cells_7x11_L1_eta",   &ph_cells_L1_eta);
    tc->Branch("ph_cells_7x11_L1_phi",   &ph_cells_L1_phi);
    tc->Branch("ph_cells_7x11_L1_etar",  &ph_cells_L1_etar);
    tc->Branch("ph_cells_7x11_L1_phir",  &ph_cells_L1_phir);
    tc->Branch("ph_cells_7x11_L1_incl",  &ph_cells_L1_incl);

    tc->Branch("ph_cells_7x11_L2_E"  ,   &ph_cells_L2_E);
    tc->Branch("ph_cells_7x11_L2_eta",   &ph_cells_L2_eta);
    tc->Branch("ph_cells_7x11_L2_phi",   &ph_cells_L2_phi);
    tc->Branch("ph_cells_7x11_L2_etar",  &ph_cells_L2_etar);
    tc->Branch("ph_cells_7x11_L2_phir",  &ph_cells_L2_phir);
    tc->Branch("ph_cells_7x11_L2_incl",  &ph_cells_L2_incl);

    tc->Branch("ph_cells_7x11_L3_E"  ,   &ph_cells_L3_E);
    tc->Branch("ph_cells_7x11_L3_eta",   &ph_cells_L3_eta);
    tc->Branch("ph_cells_7x11_L3_phi",   &ph_cells_L3_phi);
    tc->Branch("ph_cells_7x11_L3_etar",  &ph_cells_L3_etar);
    tc->Branch("ph_cells_7x11_L3_phir",  &ph_cells_L3_phir);
    tc->Branch("ph_cells_7x11_L3_incl",  &ph_cells_L3_incl);

    //
    tc->Branch("ph_L1_E",   &ph_L1_E);
    tc->Branch("ph_L2_E",   &ph_L2_E);
    tc->Branch("ph_L3_E",   &ph_L3_E);
    tc->Branch("ph_L1_eta",   &ph_L1_eta);
    tc->Branch("ph_L2_eta",   &ph_L2_eta);
    tc->Branch("ph_L3_eta",   &ph_L3_eta);
    tc->Branch("ph_L1_phi",   &ph_L1_phi);
    tc->Branch("ph_L2_phi",   &ph_L2_phi);
    tc->Branch("ph_L3_phi",   &ph_L3_phi);

    tc->Branch("ph_ss_e132", &ph_ss_e132);
    tc->Branch("ph_ss_e1152", &ph_ss_e1152);
    tc->Branch("ph_ss_ethad1", &ph_ss_ethad1);
    tc->Branch("ph_ss_ethad", &ph_ss_ethad);
    tc->Branch("ph_ss_ehad1", &ph_ss_ehad1);
    tc->Branch("ph_ss_f1", &ph_ss_f1);
    tc->Branch("ph_ss_f3", &ph_ss_f3);
    tc->Branch("ph_ss_f1core", &ph_ss_f1core);
    tc->Branch("ph_ss_f3core", &ph_ss_f3core);
    tc->Branch("ph_ss_e233", &ph_ss_e233);
    tc->Branch("ph_ss_e235", &ph_ss_e235);
    tc->Branch("ph_ss_e255", &ph_ss_e255);
    tc->Branch("ph_ss_e237", &ph_ss_e237);
    tc->Branch("ph_ss_e277", &ph_ss_e277);
    tc->Branch("ph_ss_weta1", &ph_ss_weta1);
    tc->Branch("ph_ss_weta2", &ph_ss_weta2);
    tc->Branch("ph_ss_e2ts1", &ph_ss_e2ts1);
    tc->Branch("ph_ss_e2tsts1", &ph_ss_e2tsts1);
    tc->Branch("ph_ss_fracs1", &ph_ss_fracs1);
    tc->Branch("ph_ss_widths1", &ph_ss_widths1);
    tc->Branch("ph_ss_widths2", &ph_ss_widths2);
    tc->Branch("ph_ss_poscs1", &ph_ss_poscs1);
    tc->Branch("ph_ss_poscs2", &ph_ss_poscs2);
    tc->Branch("ph_ss_asy1", &ph_ss_asy1);
    tc->Branch("ph_ss_pos", &ph_ss_pos);
    tc->Branch("ph_ss_pos7", &ph_ss_pos7);
    tc->Branch("ph_ss_barys1", &ph_ss_barys1);
    tc->Branch("ph_ss_wtots1", &ph_ss_wtots1);
    tc->Branch("ph_ss_emins1", &ph_ss_emins1);
    tc->Branch("ph_ss_emaxs1", &ph_ss_emaxs1);
    tc->Branch("ph_ss_r33over37allcalo", &ph_ss_r33over37allcalo);
    tc->Branch("ph_ss_ecore", &ph_ss_ecore);
    tc->Branch("ph_ss_Reta", &ph_ss_Reta);
    tc->Branch("ph_ss_Rphi", &ph_ss_Rphi);
    tc->Branch("ph_ss_Eratio", &ph_ss_Eratio);
    tc->Branch("ph_ss_Rhad", &ph_ss_Rhad);
    tc->Branch("ph_ss_Rhad1", &ph_ss_Rhad1);
    tc->Branch("ph_ss_DeltaE", &ph_ss_DeltaE);

    tc->Branch("ph_author", &ph_author);
    tc->Branch("ph_ambiguity_type", &ph_ambiguity_type);

    return StatusCode::SUCCESS;
}


StatusCode xAODAnalysis :: fill_cluster_info(const xAOD::Photon*& ph)
{
    if (not ph or not ph->caloCluster()) return StatusCode::FAILURE;

    const EventContext& ctx = Gaudi::Hive::currentContext();

    double eta_calo = 0.;
    if( !(ph->caloCluster()->retrieveMoment(xAOD::CaloCluster::ETACALOFRAME, eta_calo))) {
        ANA_MSG_ERROR("Failed to retrieve eta from calo");
    }
    double phi_calo = 0.;
    if( !(ph->caloCluster()->retrieveMoment(xAOD::CaloCluster::PHICALOFRAME, phi_calo))) {
        ANA_MSG_ERROR("Failed to retrieve phi from calo");
    }

    ph_calo_eta = eta_calo;
    ph_calo_phi = phi_calo;

    ph_cl_eta0 = ph->caloCluster()->eta0();
    ph_cl_phi0 = ph->caloCluster()->phi0();
    ph_cl_eta  = ph->caloCluster()->eta();
    ph_cl_phi  = ph->caloCluster()->phi();

    ph_delta_eta = std::fabs(std::fmod(eta_calo, 0.025f)) - 0.0125f;
    ph_delta_phi = std::fabs(std::fmod(phi_calo, M_PI/128)) - M_PI/256;

    std::unique_ptr<xAOD::CaloCluster> cluster;

    cluster = CaloClusterStoreHelper::makeCluster(
                                                  ph->caloCluster()->getCellLinks()->getCellContainer(),
                                                  ph->caloCluster()->eta0(),
                                                  ph->caloCluster()->phi0(),
                                                  ph->caloCluster()->clusterSize()
                                                  );
    m_calo_tool->makeCorrection(ctx, cluster.get());

    int ncell0 = 0;
    int ncell1 = 0;
    int ncell2 = 0;
    int ncell3 = 0;
    int ncell = 0;


    // Get ID fo the cells in the cluster to match with the cells in 7x11 window
    const CaloClusterCellLink* cellLinks = ph->caloCluster()->getCellLinks();

    CaloClusterCellLink::const_iterator it_cell = cellLinks->begin();
    CaloClusterCellLink::const_iterator it_cell_e = cellLinks->end();

    std::vector<std::size_t> cl_id_L1;
    std::vector<std::size_t> cl_id_L2;
    std::vector<std::size_t> cl_id_L3;

    for(;it_cell!=it_cell_e; ++it_cell) {
        const CaloCell* cell = (*it_cell);
        if (!cell)
            continue;

        int sampling = cell->caloDDE()->getSampling();

        if (sampling == CaloCell_ID::EMB1 || sampling == CaloCell_ID::EME1) {
            cl_id_L1.push_back(cell->ID().get_identifier32().get_compact());
        }
        else if (sampling == CaloCell_ID::EMB2 || sampling == CaloCell_ID::EME2) {
            cl_id_L2.push_back(cell->ID().get_identifier32().get_compact());
        }
        else if (sampling == CaloCell_ID::EMB3 || sampling == CaloCell_ID::EME3) {
            cl_id_L3.push_back(cell->ID().get_identifier32().get_compact());
        }

    }


    for (const CaloCell *cell : *cluster) {

        int sampling = cell->caloDDE()->getSampling();

        if (sampling == CaloCell_ID::PreSamplerB || sampling == CaloCell_ID::PreSamplerE) {

            ph_cells_L0_E  .push_back(cell->e());
            ph_cells_L0_eta.push_back(cell->eta());
            ph_cells_L0_phi.push_back(cell->phi());

            ncell0++;
        }

        else if (sampling == CaloCell_ID::EMB1 || sampling == CaloCell_ID::EME1) {

            ph_cells_L1_E  .push_back(cell->e());
            ph_cells_L1_eta.push_back(cell->eta());
            ph_cells_L1_phi.push_back(cell->phi());
            ph_cells_L1_etar.push_back(cell->caloDDE()->eta_raw());
            ph_cells_L1_phir.push_back(cell->caloDDE()->phi_raw());

            ph_cells_L1_incl.push_back(std::count(cl_id_L1.begin(), cl_id_L1.end(), cell->ID().get_identifier32().get_compact()));

            ncell1++;
        }

        else if (sampling == CaloCell_ID::EMB2 || sampling == CaloCell_ID::EME2) {

            ph_cells_L2_E  .push_back(cell->e());
            ph_cells_L2_eta.push_back(cell->eta());
            ph_cells_L2_phi.push_back(cell->phi());
            ph_cells_L2_etar.push_back(cell->caloDDE()->eta_raw());
            ph_cells_L2_phir.push_back(cell->caloDDE()->phi_raw());

            ph_cells_L2_incl.push_back(std::count(cl_id_L2.begin(), cl_id_L2.end(), cell->ID().get_identifier32().get_compact()));

            ncell2++;
        }

        else if (sampling == CaloCell_ID::EMB3 || sampling == CaloCell_ID::EME3) {

            ph_cells_L3_E  .push_back(cell->e());
            ph_cells_L3_eta.push_back(cell->eta());
            ph_cells_L3_phi.push_back(cell->phi());
            ph_cells_L3_etar.push_back(cell->caloDDE()->eta_raw());
            ph_cells_L3_phir.push_back(cell->caloDDE()->phi_raw());

            ph_cells_L3_incl.push_back(std::count(cl_id_L3.begin(), cl_id_L3.end(), cell->ID().get_identifier32().get_compact()));

            ncell3++;
        }

        ncell++;
    }

    ph_cells_L0_size = ncell0;
    ph_cells_L1_size = ncell1;
    ph_cells_L2_size = ncell2;
    ph_cells_L3_size = ncell3;

    // Check number of cluster cells (only valid for barrel and with some exceptions)
    bool is_L1_ok = (ph_cells_L1_size >= 56);
    bool is_L2_ok = (ph_cells_L2_size >= 77);
    bool is_L3_ok = (ph_cells_L3_size >= 44);

    if (is_L1_ok) hist("h_counts")->Fill(3.5);
    if (is_L2_ok) hist("h_counts")->Fill(4.5);
    if (is_L3_ok) hist("h_counts")->Fill(5.5);

    if (is_L1_ok && is_L2_ok && is_L3_ok)
        hist("h_counts")->Fill(6.5);

    return StatusCode::SUCCESS;
}

StatusCode xAODAnalysis::read_events_file(unsigned int run_number)
{
    m_selected_events.clear();
    m_selected_photons.clear();

    TString events_file = Form("%s", m_events_file.c_str());

    if (m_isMC)
        ANA_MSG_INFO("Reading events from file " << events_file.Data());
    else
        ANA_MSG_INFO("Reading events from file " << events_file.Data() << " with RunNumber = " << run_number);

    std::ifstream infile(events_file.Data());

    unsigned int run;
    unsigned long long event;
    float eta, phi;

    if (m_isMC) {
        while (infile >> event >> eta >> phi) {

            m_selected_events.push_back(event);

            MatchPhoton p { eta, phi };

            m_selected_photons[event] = p;
        }
    }
    else {
        while (infile >> run >> event >> eta >> phi) {

            if (run != run_number)
                continue;

            m_selected_events.push_back(event);

            MatchPhoton p { eta, phi };

            m_selected_photons[event] = p;
        }
    }

    ANA_MSG_INFO("Loaded " << m_selected_events.size() << " events from file");

    return StatusCode::SUCCESS;
}
