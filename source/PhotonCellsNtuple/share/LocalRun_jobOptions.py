# PhotonCellsNtuple: JO for local test

#override next line on command line with: --filesInput=XXX
#test_file = '/eos/user/f/falonso/data/local_test/mc20_13TeV.700011.Sh_228_eegamma_pty7_EnhMaxpTVpTy.deriv.DAOD_EGAM3.e7947_s3681_r13145_r13146_p4940/DAOD_EGAM3.27820012._000028.pool.root.1'
test_file = '/eos/user/f/falonso/data/local_test/mc20_13TeV.800662.Py8_gammajet_direct_DP70_140_ordered.recon.AOD.e8279_s3681_r13145/AOD.27612012._000026.pool.root.1'

jps.AthenaCommonFlags.FilesInput = [test_file]

# Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "POOLAccess"

# configure detector description from metadata in input file
from RecExConfig import AutoConfiguration
AutoConfiguration.ConfigureSimulationOrRealData()
AutoConfiguration.ConfigureGeo()
AutoConfiguration.ConfigureConditionsTag()

from AthenaCommon.DetFlags import DetFlags
DetFlags.detdescr.all_setOff()
DetFlags.BField_setOn()
DetFlags.digitize.all_setOff()
DetFlags.detdescr.Calo_setOn()
DetFlags.simulate.all_setOff()
DetFlags.pileup.all_setOff()
DetFlags.overlay.all_setOff();
include("RecExCond/AllDet_detDescr.py")

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ('xAODAnalysis', 'AnalysisAlg')
alg.EventsFile = events_file
alg.IsMC = isMC

# CaloCluster tool
from egammaRec import egammaKeys
from egammaRec.Factories import ToolFactory
from CaloClusterCorrection.CaloClusterCorrectionConf import CaloFillRectangularCluster as CFRC
CaloFillRectangularCluster = ToolFactory(CFRC,
                                         name="CaloFillRectangularCluster_7x11")

ToolSvc = [ CaloFillRectangularCluster(eta_size=7, phi_size=11, fill_cluster=True) ] #.copy(name = "CaloFillRectangularCluster_7x11", eta_size=7, phi_size=11)() ]

# Output
jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:xAODAnalysis.outputs.root"]
svcMgr.THistSvc.MaxFileSize = -1

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
theApp.EvtMax = 500

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")
