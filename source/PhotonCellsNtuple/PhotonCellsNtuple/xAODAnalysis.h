#ifndef xAODAnalysis_H
#define xAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include <xAODEventInfo/EventInfo.h>

//Tools include(s) :
#include <AsgTools/ToolHandle.h>
#include "StoreGate/ReadHandleKey.h"

//ROOTCORE include(s) :
#include "xAODCore/ShallowCopy.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/EgammaxAODHelpers.h"


//Athena include(s) :
// #include "CaloEvent/CaloClusterCellLink.h"
#include "CaloEvent/CaloCellContainer.h"
#include "CaloUtils/CaloClusterStoreHelper.h"
#include "CaloUtils/CaloCellDetPos.h"
#include "CaloClusterCorrection/CaloFillRectangularCluster.h"

struct MatchPhoton {
    double eta;
    double phi;
};


class xAODAnalysis : public EL::AnaAlgorithm {

public:
    // this is a standard algorithm constructor
    xAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

    StatusCode   read_events_file(unsigned int);
    StatusCode   fill_cluster_info(const xAOD::Photon*& egamma);
    StatusCode   clear_variables();
    StatusCode   setup_tree();

private:

    ToolHandle<CaloFillRectangularCluster> m_calo_tool;

    SG::ReadHandleKey<CaloCellContainer> m_SGKey_caloCells{
                                                           this,
                                                           "SGKey_caloCells",
                                                           "AllCalo",
                                                           "SG key of the cell container"
    };

    std::vector< const xAOD::Photon* > m_photons;

    unsigned int m_previous_run_number;
    std::string m_events_file;
    bool m_isMC;

    /// List of selected events
    std::vector<unsigned long long> m_selected_events;
    std::map<unsigned long long, MatchPhoton> m_selected_photons;

    unsigned int run_number;
    unsigned long long event_number;
    // Int_t       mc_dsid;

    double ph_pt;
    double ph_eta;
    double ph_phi;

    double ph_cl_eta0;
    double ph_cl_phi0;
    double ph_cl_eta;
    double ph_cl_phi;
    double ph_calo_eta;
    double ph_calo_phi;
    double ph_delta_eta;
    double ph_delta_phi;

    int ph_cells_L0_size;
    int ph_cells_L1_size;
    int ph_cells_L2_size;
    int ph_cells_L3_size;

    std::vector<double> ph_cells_L0_E;
    std::vector<double> ph_cells_L0_eta;
    std::vector<double> ph_cells_L0_phi;

    std::vector<double> ph_cells_L1_E;
    std::vector<double> ph_cells_L1_eta;
    std::vector<double> ph_cells_L1_phi;
    std::vector<double> ph_cells_L1_etar;
    std::vector<double> ph_cells_L1_phir;
    std::vector<bool>   ph_cells_L1_incl;

    std::vector<double> ph_cells_L2_E;
    std::vector<double> ph_cells_L2_eta;
    std::vector<double> ph_cells_L2_phi;
    std::vector<double> ph_cells_L2_etar;
    std::vector<double> ph_cells_L2_phir;
    std::vector<bool>   ph_cells_L2_incl;

    std::vector<double> ph_cells_L3_E;
    std::vector<double> ph_cells_L3_eta;
    std::vector<double> ph_cells_L3_phi;
    std::vector<double> ph_cells_L3_etar;
    std::vector<double> ph_cells_L3_phir;
    std::vector<bool>   ph_cells_L3_incl;

    // E/eta/phi in each layer
    double ph_L1_E, ph_L1_eta, ph_L1_phi;
    double ph_L2_E, ph_L2_eta, ph_L2_phi;
    double ph_L3_E, ph_L3_eta, ph_L3_phi;

    /// Shower shape details
    /// uncalibrated energy (sum of cells) in strips in a 3x2 window in cells in eta X phi
    double  ph_ss_e132;
    /// uncalibrated energy (sum of cells) in strips in a 15x2 window in cells in eta X phi
    double  ph_ss_e1152;
    /// transverse energy in the first sampling of the hadronic calorimeters behind the cluster calculated from ehad1
    double  ph_ss_ethad1;
    /// ET leakage into hadronic calorimeter with exclusion of energy in CaloSampling::TileGap3
    double  ph_ss_ethad;
    /// E leakage into 1st sampling of had calo (CaloSampling::HEC0 + CaloSampling::TileBar0 + CaloSampling::TileExt0)
    double  ph_ss_ehad1;
    /// E1/E = fraction of energy reconstructed in the first sampling, where E1 is energy in all strips belonging to the cluster and E is the total energy reconstructed in the electromagnetic calorimeter cluster
    double  ph_ss_f1;
    /// fraction of energy reconstructed in 3rd sampling
    double  ph_ss_f3;
    /// E1(3x1)/E = fraction of the energy reconstructed in the first longitudinal compartment of the electromagnetic calorimeter, where E1(3x1) the energy reconstructed in +/-3 strips in eta, centered around the maximum energy $
    double  ph_ss_f1core;
    /// E3(3x3)/E fraction of the energy reconstructed in the third compartment of the electromagnetic calorimeter, where E3(3x3), energy in the back sampling, is the sum of the energy contained in a 3x3 window around the maximu$
    double  ph_ss_f3core;
    /// uncalibrated energy (sum of cells) of the middle sampling in a rectangle of size 3x3 (in cell units eta X phi)
    double  ph_ss_e233;
    /// uncalibrated energy (sum of cells) of the middle sampling in a rectangle of size 3x5
    double  ph_ss_e235;
    /// uncalibrated energy (sum of cells) of the middle sampling in a rectangle of size 5x5
    double  ph_ss_e255;
    ///  uncalibrated energy (sum of cells) of the middle sampling in a rectangle of size 3x7
    double  ph_ss_e237;
    /// uncalibrated energy (sum of cells) of the middle sampling in a rectangle of size 7x7
    double  ph_ss_e277;
    /// shower width using +/-3 strips around the one with the maximal energy deposit:
    /// w3 strips = sqrt{sum(Ei)x(i-imax)^2/sum(Ei)}, where i is the number of the strip and imax the strip number of the most energetic one
    double  ph_ss_weta1;
    /// the lateral width is calculated with a window of 3x5 cells using the energy weighted  sum over all cells, which depends on the particle impact point inside the cell: weta2 =
    /// sqrt(sum Ei x eta^2)/(sum Ei) -((sum Ei x eta)/(sum Ei))^2, where Ei is the energy of the i-th cell
    double  ph_ss_weta2;
    /// 2nd max in strips calc by summing 3 strips
    double  ph_ss_e2ts1;
    /// energy of the cell corresponding to second energy maximum in the first sampling
    double  ph_ss_e2tsts1;
    /// shower shape in the shower core : [E(+/-3)-E(+/-1)]/E(+/-1), where E(+/-n) is the energy in +- n strips around the strip with highest energy
    double  ph_ss_fracs1;
    /// same as egammaParameters::weta1 but without corrections  on particle impact point inside the cell
    double  ph_ss_widths1;
    /// same as egammaParameters::weta2 but without corrections on particle impact point inside the cell
    double  ph_ss_widths2;
    /// relative position in eta within cell in 1st sampling
    double  ph_ss_poscs1;
    /// relative position in eta within cell in 2nd sampling
    double  ph_ss_poscs2;
    /// uncorr asymmetry in 3 strips in the 1st sampling
    double  ph_ss_asy1;
    /// difference between shower cell and predicted track in +/- 1 cells
    double  ph_ss_pos;
    /// Difference between the track and the shower positions:
    /// sum_{i=i_m-7}^{i=i_m+7}E_i x (i-i_m) / sum_{i=i_m-7}^{i=i_m+7}E_i,
    /// The difference between the track and the shower positions measured
    /// in units of distance between the strips, where i_m is the impact cell
    /// for the track reconstructed in the inner detector and E_i is the energy
    /// reconstructed in the i-th cell in the eta direction for constant phi given by the track parameters
    double  ph_ss_pos7;
    ///  barycentre in sampling 1 calculated in 3 strips
    double  ph_ss_barys1;
    /// shower width is determined in a window detaxdphi = 0,0625 x~0,2, corresponding typically to 20 strips in eta : wtot1=sqrt{sum Ei x ( i-imax)^2 / sum Ei}, where i is the strip number and imax the strip number of the first$
    double  ph_ss_wtots1;
    /// energy reconstructed in the strip with the minimal value between the first and second maximum
    double  ph_ss_emins1;
    /// energy of strip with maximal energy deposit
    double  ph_ss_emaxs1;
    ///  1-ratio of energy in 3x3 over 3x7 cells;
    /// E(3x3) = E0(1x1) + E1(3x1) + E2(3x3) + E3(3x3); E(3x7) = E0(3x3) + E1(15x3) + E2(3x7) + E3(3x7)
    double ph_ss_r33over37allcalo;
    /// core energy in em calo  E(core) = E0(3x3) + E1(15x2) + E2(5x5) + E3(3x5)
    double ph_ss_ecore;
    /// pointing z at vertex reconstructed from the cluster
    /// e237/e277
    double ph_ss_Reta;
    ///  e233/e237
    double ph_ss_Rphi;
    ///  (emaxs1-e2tsts1)/(emaxs1+e2tsts1)
    double ph_ss_Eratio;
    /// ethad/et
    double ph_ss_Rhad;
    ///  ethad1/et
    double ph_ss_Rhad1;
    /// e2tsts1-emins1
    double ph_ss_DeltaE;

    // tmp variables (until they are added to official ntuples)
    int ph_author;
    int ph_ambiguity_type;
};

#endif
