# PhotonCellsNtuple

# Setup

    source setup.sh

# Compile

    mkdir build
    cd buid
    cmake ../source
    make
    source x86_64-centos7-gcc62-opt/setup.sh


# Prepare input file with events

The first step is to create the files with the event number and photon information for the events passing a minimal selection from the official ntuples


# Local test

    athena -c "events_file=eventsXXX.txt; isMC=True;" LocalRun_jobOptions.py

# To run on the grid:

    pathena -c "events_file=eventsXXX.txt; isMC=True;" GridRun_jobOptions.py --inDS={input}  --outDS={output} --nGBPerJob=MAX --extFile=eventsXXX.txt


where eventsXXX.txt is the file with the events generated in the previous step for the corresponding input sample. You need to change also the isMC flag.
